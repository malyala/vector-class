/*
Name : Vector.cpp
Author: Amit Malyala, Copyright Amit Malyala 2019- Current.
Description:
A class.
Version: 0.1
Notes:
A data structure similar to a vector.
*/
#include "Vector.h"

int main(void)
{
	
	Vector<int>  myVec {1,2,3,4,5,6} ;
	
	for (long int i=0;i< 999;i++)
	{
		myVec.push_back(i);
	}
	std::size_t count= myVec.size();
	
	for (std::size_t i=0;i<count;i++ )
	{
		std::cout << myVec[i] << std::endl;
	}
	return 0;
}