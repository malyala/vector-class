/*
Name : Vector.h
Author: Amit Malyala, Copyright Amit Malyala 2019- Current.
Description:
A class.
Version: 
0.1  Initial version
0.2  Changed constructor 
Notes:
A data structure similar to a vector.
SPDX License identifer: Apache-2.0

To do:
add reserve function to allocate memory.

Notes:

about using std::move in the overloaded assignment operator. Now, this can generally be an issue. Consider your T is of class-type 
and the class-type contains a member like std::mutex. The things wont be movable, nor copyable. I'd probably you a template switch here 
to get a more narrow contract with client code and signal come useful message. But okay, that'd be compile-time error either way.

 About Vector::push_back. Member function std::vector::push_back is quite special. I saw you're copying your data there. 
 std::vector::push_back doesn't necessary do that. It uses std::move_if_no_except. If T has a move ctor with a strong exception safety guarantee, 
 it does not need to worry that an exception might be thrown. If this would happen, and std::vector would move, the previous memory location 
 of the object T might not be available
 anymore. So it couldn't revert changes. So if you can guarantee a noexcept move ctor for T, std::vector::push_back will move, otherwise it will 
 copy. It's an optimization.
  
 also from a semantic point of view not yet comparable to std::vector, but IMO, I don't thing your Vector should be a competition to std::vector 
 in any way.  Reimplementing things from stdlib is part of learning. I do it all the time. Sometimes, I don't look at stdlib 
 implementation. I only read the semantics provided and then write the thing to mimic it as closely to the semantics as possible. Then compare 
 it to libc++ for instance.
 
  write copy assignment operator so you don't need the `if(this != &otherVector)` check, then it'll probably be exception safe
 
*/
#include <cstdlib>
#include <cstdarg>
#include <iostream>
#include <algorithm>


template <typename T>
class Vector 
{
	private:
		T* ptr; 
		std::size_t Count;
		
	public:
		/* Constructor */
		Vector() : Count(0), ptr(nullptr) {} 
		
		/* Use variadic templates to define a initialiazer list type constructor with variable number of elements */
		void push_back(const T& element)
		{
			
			T* ptrnew= new T[Count+1];
			for (std::size_t i=0;i<Count;i++ )
			{
				ptrnew[i]=ptr[i];
			}
			ptrnew[Count] = element;
			Count++;
			delete[] ptr;
			ptr=nullptr;
			ptr=ptrnew;
		}

		T& back(void)
		{
			return ptr[Count-1];
		}
		void pop_back(void)
		{
			/* function being edited */
			Count--;
			T* ptrnew = new T[Count];
			for (std::size_t i=0;i<Count;i++ )
			{
				ptrnew[i]=ptr[i];
			}
			delete[] ptr;
			ptr= ptrnew;
		}
	
		template< typename... Args>
        Vector<T> (Args&&... args)
        {
            /* Add each argument into a temporary buffer and create a dynamic array ptr with that size.
            Unpack argument list and add each argument into ptr[]; 
            
            Typical initialization is Vector<int> a{1,2,3,4,5,6};
            */
            Count= sizeof...(Args);
            ptr = new T[Count] {args...};
        }
		/* copy construcotr */
		Vector (const Vector& right )
        {
        	
        	this->clear();
    	   Count=right.Count;
    	   //Allocate memory here 
    	   ptr = new T[Count];
    	   for (int i=0;i<Count;i++)
		   {
		   	   ptr[i]=(right.ptr[i]); 
		   }
    	   
	    }
         
         /* Copy = Operator . Make this exception safe for new[] throw conditions.*/
         Vector& operator= (const Vector& otherVector)
         {
         	if (this!= otherVector)
         	{
         		this->clear();
				Count=otherVector.Count;
				ptr = new T[Count];
				for (std::size_t i=0;i<Count;i++)
			    {
				    ptr[i]=otherVector.ptr[i];				
			    }
			}
			
			return *this;
		 }
         
         
		/* Move constructor */
		
		Vector (const Vector&& right )
        {
        	
        	this->clear();
    	   Count=right.Count;
    	
    	   //Allocate memory here 
    	   ptr=std::move(right.ptr);
		   right.ptr = nullptr;
	    }
		/* Move assignment constructor */
		
		 /* Copy = Operator */
         Vector& operator= (const Vector&& otherVector)
         {
         	if (this!= otherVector)
         	{
         		this->clear();
				Count=otherVector.Count;
				//. do copy instead of move 
			    ptr= std::move(otherVector.ptr);
			    
			    otherVector.ptr=nullptr;
			}
			
			return *this;
		 }
         
		/* For accessing a vector element. Check correct syntax of vector  */
		T& operator[] (std::size_t i) const
		{
			return ptr[i];
		}
		/* get number of elements in Vector */		
		std::size_t size(void) const
		{
			return Count;
		}
		
		/* erases vector */
		void clear(void)
		{
			if (Count>=1)
			{
				delete[] ptr; 
				ptr=nullptr;
			    Count=0;
			}
			
			
		}
		/* Destructor */
		~Vector()
		{
			if (Count>=1)
			{
				delete[] ptr; 
				ptr=nullptr;
			    Count=0;
			}
		}
		
		
};